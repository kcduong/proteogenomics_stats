# Analysing Peptide Expression in Proteomic Cancer data. 

A modified version of an existing R script (Brandsma C-A, et al. Thorax 2019).  
This script analyses and identifies peptides of interest in cancer and control samples, with old and young subgroups. 

Peptides with PSMs exclusively expressed in only one of the two groups are visualized in bar plots.  
Genes of these peptides are checked for relation to different types of cancers.

## Getting Started

There are two scripts that can be run: "RNAseq_stats_CancerControl.R" and "RNAseq_stats_CancerControl.R"

RNAseq_stats_CancerControl.R is suitable for Cancer vs. Control comparison.  
RNAseq_stats_OldYoung.R is suitable for Old vs. Young comparison of either cancer samples or control samples, depending  
on what you use as input file. The script currently assumes cancer samples as input and will name graphs and such to  
represent that.

The input file and other constants can be modified at the top of the scripts. 

### Prerequisites
These scripts also require the directory of a blast database of a human-filtered SwissProt protein database. Filtering   
the database on humans can be done with the  human_only_db.py that can be found in this [repository](https://bitbucket.org/kcduong/proteogenomics/)

### Run
After inputs and constants have been altered to suit the data, simply run the entire R script.  
The output can be found in the output/ folder. 

### Built with
R version 3.6.2  
NCBI Blast+ version 2.9.0

And the following R packages:
```
| Package       | Version      |
|---------------|--------------|
|    Seqinr     |    3.6.1     |
|    Peptides   |    2.4.1     |
|    gProfileR  |    0.7.0     |
|    STRINGdb   |    1.26.0    |
|    biomaRt    |    2.42.0    |
|    annotate   |    1.64.0    |
|    hpar       |    1.28      |
```